#!/usr/bin/env python

# -*- coding:utf-8 -*-

# @Time      :2022-5-3 23:30

# @Author    :Mengxh

# @File      :mir3_is_item.py

import pyautogui
import mir3_init
import threading


def is_item(item, pos_list):
    t = pyautogui.locateOnScreen('%s' % item, region=mir3_init.region_main, confidence=0.6)
    if t is not None:
        pos = pyautogui.center(t)
        print(item,pos)
        pos_list.append(pos)

def multi_thread():
    print('start')
    threads = []
    pos_list= []
    for item in mir3_init.itemlist:
        threads.append(
            threading.Thread(target=is_item, args=(item, pos_list))
        )

    for thread in threads:
        thread.start()

    for thread in threads:
        thread.join()

    print('end')
    return pos_list

if __name__ == '__main__':
    multi_thread()
