#!/usr/bin/env python

# -*- coding:utf-8 -*-

# @Time      :2022-5-4 0:08

# @Author    :Mengxh

# @File      :mir3_init.py
import os

# 装备图库载入列表
itemlist = []
for root, dirs, files in os.walk("../pics/pickup", topdown=False):
    for name in files:
        itemlist.append(os.path.join(root, name))

# 定义逐点行走方向和鼠标左右键
l = 350  # x = 100左
r = 700  # x = 900右
u = 200  # y = 150上
d = 500  # y = 550下
role_x = 502  # 人物中心点x坐标
role_y = 356  # 人物中心点y坐标
lu = (l, u)  # 左上
ld = (l, d)  # 左下
ru = (r, u)  # 左上
rd = (r, d)  # 左上
move_list_u = [lu, ru]
move_list_d = [ld, rd]
move_list = move_list_d + move_list_u
region_0 = (1052, 67, 362, 214)  # 大补贴截屏范围
region_main = (21, 51, 1000, 580)  # 游戏主画面范围
print('行走方向设置完成')
print('主游戏画面区域设置完成')

if __name__ == '__main__':
    print(itemlist)