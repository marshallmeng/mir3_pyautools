#!/usr/bin/env python

# -*- coding:utf-8 -*-

# @Time      :2022-5-4 0:25

# @Author    :Mengxh

# @File      :mir3_main.py
import pyautogui
import time
import mir3_is_item, mir3_pickup, mir3_is_enemy, mir3_init


# pyautogui.PAUSE = 1  # 看法术释放是否正常

# 回收钻石（完成）
def zuanshi():
    print('操作D菜单回收钻石')
    pyautogui.press('d')
    t = pyautogui.locateOnScreen('../pics/menupng/jinbi1.png', region=mir3_init.region_main, confidence=0.8)
    if t is not None:
        pos = pyautogui.center(t)
        pyautogui.click(pos.x, pos.y)
    t = pyautogui.locateOnScreen('../pics/menupng/jinbi2.png', region=mir3_init.region_main, confidence=0.8)
    if t is not None:
        pos = pyautogui.center(t)
        pyautogui.click(pos.x, pos.y)
    t = pyautogui.locateOnScreen('../pics/menupng/jinbi3.png', region=mir3_init.region_main, confidence=0.8)
    if t is not None:
        pos = pyautogui.center(t)
        pyautogui.click(pos.x, pos.y)
    t = pyautogui.locateOnScreen('../pics/menupng/zuanshihuishou1.png', region=mir3_init.region_main, confidence=0.8)
    if t is not None:
        pos = pyautogui.center(t)
        pyautogui.click(pos.x, pos.y)
    t = pyautogui.locateOnScreen('../pics/menupng/zuanshihuishou2.png', region=mir3_init.region_main, confidence=0.8)
    if t is not None:
        pos = pyautogui.center(t)
        pyautogui.click(pos.x, pos.y)

# 特修装备（完成）
def repair():
    print('装备特修')
    pyautogui.press('d')
    t = pyautogui.locateOnScreen('../pics/menupng/texiu1.png', region=mir3_init.region_main, confidence=0.8)
    if t is not None:
        pos = pyautogui.center(t)
        pyautogui.click(pos.x, pos.y)

    t = pyautogui.locateOnScreen('../pics/menupng/texiu2.png', region=mir3_init.region_main, confidence=0.8)
    if t is not None:
        pos = pyautogui.center(t)
        pyautogui.click(pos.x, pos.y)

# 随身买药（完成）
def buymed(n):
    print('随身买药')
    pyautogui.press('d')
    t = pyautogui.locateOnScreen('../pics/menupng/suishen1.png', region=mir3_init.region_main, confidence=0.8)
    if t is not None:
        pos = pyautogui.center(t)
        pyautogui.click(pos.x, pos.y)

    t = pyautogui.locateOnScreen('../pics/menupng/suishen2.png', region=mir3_init.region_main, confidence=0.8)
    if t is not None:
        pos = pyautogui.center(t)
        pyautogui.click(pos.x, pos.y)
        pyautogui.typewrite(str(n))
        pyautogui.press('enter')

    t = pyautogui.locateOnScreen('../pics/menupng/suishen3.png', region=mir3_init.region_main, confidence=0.8)
    if t is not None:
        pos = pyautogui.center(t)
        pyautogui.click(pos.x, pos.y)


def main():
    j = 1
    list_pos = []  # 监控传递坐标是否相同
    while j:
        # 1. 判断怪物
        # 1.1 如是
        time.sleep(1)
        if mir3_is_enemy.is_enemy():
            # 持续攻击F2
            k = 1
            while k:
                pyautogui.press('f2', interval=0.5)
                pyautogui.press('f2', interval=0.5)
                im_1 = pyautogui.locateOnScreen('../pics/menupng/kouxue.png', region=mir3_init.region_main, confidence=0.9)
                im_2 = pyautogui.locateOnScreen('../pics/menupng/kouxue.png', region=mir3_init.region_main, confidence=0.9)
                if im_1 or im_2:
                    print('打了有怪')
                else:
                    print('打了无怪')
                    k = 0

        # 2. 判断捡东西
        print('进入捡东西')
        # 获取东西坐标
        pos_dict = mir3_is_item.multi_thread()
        if not pos_dict:
            j = 0
            break
        # 计算每个坐标点与角色坐标的直线距离，加入字典准备对比
        s_dict = {}
        for pos in pos_dict:
            s = ((pos.x - mir3_init.role_x) ** 2 + (pos.y - mir3_init.role_y) ** 2) ** 0.5
            s_dict[pos] = s
        # 获得最近距离的坐标点
        pos = min(s_dict, key=s_dict.get)
        list_pos.append(pos)
        if len(set(list_pos)) < len(list_pos) - 6:
            print('坐标卡死，中止')
            break

        if len(set(list_pos)) < len(list_pos) - 3:
            print('坐标卡死，回收钻石')
            zuanshi()

        # 循环走到坐标点
        mir3_pickup.pickup(pos)



if __name__ == '__main__':
    input("按回车键启动程序")
    i = 1
    while True:
        main()

        if i % 9 == 0:
            zuanshi()

        # 定期修理装备
        if i % 50 == 0:
            repair()

        # 随身买药
        if i % 800 == 0:
            buymed(100)

        print('瞬息移动')
        pyautogui.press('f5')


        i += 1
        print('i=',i)
