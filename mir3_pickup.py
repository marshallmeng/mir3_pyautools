#!/usr/bin/env python

# -*- coding:utf-8 -*-

# @Time      :2022-5-4 0:01

# @Author    :Mengxh

# @File      :mir3_pickup.py
import pyautogui
import mir3_init, mir3_is_item

pyautogui.PAUSE = 1


def pickup(pos):
    print('接近待捡取物品', pos.x, pos.y)
    item_x = pos.x
    item_y = pos.y
    list_pos = []
    delta_x = 48
    delta_y = 32
    while True:
        if abs(item_x - mir3_init.role_x) <= 14 and abs(item_y - mir3_init.role_y) <= 14:
            break
        # 判断物品坐标与人物的相对方向，定义行走方向
        if item_x < mir3_init.role_x - 14:
            x = mir3_init.l
            item_x += delta_x
        elif item_x > mir3_init.role_x + 14:
            x = mir3_init.r
            item_x -= delta_x
        else:
            x = mir3_init.role_x
        if item_y <= mir3_init.role_y - 5:
            y = mir3_init.u
            item_y += delta_y
        elif item_y >= mir3_init.role_y + 5:
            y = mir3_init.d
            item_y -= delta_y
        else:
            y = mir3_init.role_y

        list_pos.append((item_x,item_y))

        button = 'LEFT'
        # #根据物品远近判断用左右键鼠标行走
        # if ((mir3_init.role_x - pos.x) ** 2 + (mir3_init.role_y - pos.y) ** 2) ** 0.5 >= 105:
        #     button = 'RIGHT'
        #     delta_x *= 2
        #     delta_y *= 2
        # else:
        #     button = 'LEFT'


        # 向装备移动
        pyautogui.click(x=x, y=y, button=button)
        print('item x,y',item_x,item_y)
        print('点击',x,y,button)

        # 防卡死
        if len(set(list_pos)) < len(list_pos) - 3:
            print('坐标卡死，中止')
            print(list_pos)
            break

if __name__ == '__main__':
    pickup(mir3_is_item.multi_thread()[0])
