#!/usr/bin/env python

# -*- coding:utf-8 -*-

# @Time      :2022-5-3 23:49

# @Author    :Mengxh

# @File      :mir3_is_enemy.py
import time
import pyautogui
import mir3_init
pyautogui.PAUSE = 1


def is_enemy():
    t = pyautogui.locateOnScreen('../pics/locatepng/guaiwu.png', region=mir3_init.region_0, confidence=0.7)
    if t is not None:
        for i in range(2):
            pyautogui.press('f2', interval=0.5)
        im_1 = pyautogui.locateOnScreen('../pics/menupng/kouxue.png', region=mir3_init.region_main, confidence=0.9)
        im_2 = pyautogui.locateOnScreen('../pics/menupng/kouxue.png', region=mir3_init.region_main, confidence=0.9)
        if im_1 or im_2:
            print('打了有怪')
            return True
        else:
            print('打了无怪')
            return False
    else:
        print('信息无怪')
        return False


if __name__ == '__main__':
    time.sleep(2)
    is_enemy()
